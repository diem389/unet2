from keras.callbacks import ModelCheckpoint

from data import *
from model import unet

MODEL_FILE = "unet_membrane.hdf5"

img_aug_args = dict(rotation_range=0.2,
                    width_shift_range=0.05,
                    height_shift_range=0.05,
                    shear_range=0.05,
                    zoom_range=0.05,
                    horizontal_flip=True,
                    fill_mode='nearest')


def check_generator():
    train_gen = train_generator(10, 'data/membrane/train', 'image', 'label',
                                img_aug_args, save_to_dir='data/aug_test/')
    for batch in train_gen:
        break



def train_unet():
    train_gen = train_generator(2, 'data/membrane/train', 'image', 'label',
                                img_aug_args, save_to_dir=None)

    model = unet()
    model_checkpoint = ModelCheckpoint(MODEL_FILE, monitor='loss',
                                       verbose=1, save_best_only=True)

    model.fit_generator(train_gen, steps_per_epoch=2000,
                        epochs=5, callbacks=[model_checkpoint])


def predict_test():
    test_gene = test_generator("data/membrane/test")

    model = unet()
    model.load_weights(MODEL_FILE)
    results = model.predict_generator(test_gene, 30, verbose=1)
    save_result("data/membrane/test2", results)


if __name__ == "__main__":
    check_generator()
    # train_unet()
    # predict_test()
