from keras.callbacks import ModelCheckpoint

from data import *
from model import unet

MODEL_FILE = "unet_carvana.h5"
DATA_FOLDER = "/home/jaeyoon/data/carvana_small/train/"
TEST_FOLDER = "/home/jaeyoon/data/carvana_small/test/image/"
TEST_RESULT = "/home/jaeyoon/data/carvana_small/test/result/"

img_aug_args = dict(rotation_range=0.2,
                    width_shift_range=0.05,
                    height_shift_range=0.05,
                    shear_range=0.05,
                    zoom_range=0.05,
                    horizontal_flip=True,
                    fill_mode='nearest')

TARGET_SIZE= (240, 320)

def check_generator():
    train_gen = train_generator(10, DATA_FOLDER, 'image', 'label', img_aug_args, image_color_mode='rgb',
                                mask_color_mode='grayscale', save_to_dir=DATA_FOLDER + 'aug_test',
                                target_size=TARGET_SIZE)

    # just see one batch
    for batch in train_gen:
        break


def train_unet():
    train_gen = train_generator(2, DATA_FOLDER, 'image', 'label',
                                img_aug_args, save_to_dir=None, image_color_mode='rgb',
                                mask_color_mode='grayscale', target_size= TARGET_SIZE)

    model = unet(input_size= TARGET_SIZE +(3,))
    model_checkpoint = ModelCheckpoint(MODEL_FILE, monitor='loss',
                                       verbose=1, save_best_only=True)

    model.fit_generator(train_gen, steps_per_epoch=2000,
                        epochs=3, callbacks=[model_checkpoint])



def check_generator2():
    test_gene = test_generator2(TEST_FOLDER, target_size=TARGET_SIZE)

    for i, batch in enumerate(test_gene):
        f_name = f"{i}.jpg"
        f_path = os.path.join(TEST_RESULT, f_name)
        img = np.reshape(batch, batch.shape[1:])
        io.imsave(f_path, img)
        print(f"{i+1} image tested.")


def predict_test():
    test_gene = test_generator2(TEST_FOLDER, target_size=TARGET_SIZE)

    model = unet(input_size= TARGET_SIZE +(3,))
    model.load_weights(MODEL_FILE)
    results = model.predict_generator(test_gene, 38, verbose=1)
    save_result(TEST_RESULT, results)

def datagen_test():
    dg = ImageDataGenerator(img_aug_args)
    dg1 = dg.flow_from_directory(DATA_FOLDER,
                                 batch_size=10,
                                 classes=['image'],
                                 class_mode=None,
                                 color_mode="rgb",
                                 save_to_dir=DATA_FOLDER + 'gen_test',
                                 target_size=TARGET_SIZE,
                                 seed=1)
    for dummy in dg1:
        break

if __name__ == "__main__":

    # datagen_test()
    # check_generator2()

    # train_unet()
    predict_test()
